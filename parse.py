from pyNfsClient import (Portmap, Mount, NFSv3, MNT3_OK, NFS_PROGRAM,NFS_V3, NFS3_OK, DATA_SYNC)
import json
import socket
import re
from netaddr import IPNetwork
from ping3 import ping
import requests
import datetime
import argparse

def flush(host, opensearchurl):
        for share in host["shares"]:
            print("Updating share: "+share["identifier"])
            response = requests.put(f"{opensearchurl}/_doc/{share['identifier']}", data=json.dumps(share), headers={"content-type": "application/json", 'charset':'UTF-8'})
            if (response.status_code < 200 or response.status_code > 300) and response.status_code != 409:
                print(f"Error sending data to opensearch: {str(response.text)}")

def delete_old_shares(opensearchurl):
    myjson = {
                "query": {
                        "bool": {
                            "must_not": [
                                {
                                "range": {
                                "last_seen": {
                                "lt": "now-30d/d"
                                }
                                }
                            }
                            ]
                        }
                    }
                }
    response = requests.post(f"{opensearchurl}/_delete_by_query", data=json.dumps(myjson), headers={"content-type": "application/json", 'charset':'UTF-8'})
    if response.status_code == 200:
        print("Deleting old records succeeded")
    if response.status_code != 200:
        print("Deleting old records FAILED")

def groupdrill(input, groups):
    if len(input) != 0:
        groups.append(input[0].gr_name.decode('utf-8'))
        groupdrill(input[0].gr_next, groups)

def exportdrill(input, dirs):
    if len(input) != 0:
        groups = []
        groupdrill(input[0].ex_groups, groups)
        dirs.append({
            "share" : {
            "name": input[0].ex_dir.decode('utf-8'),
            "clients" : groups
            }
            })
        exportdrill(input[0].ex_next, dirs)

def get_exports(host):
    try:
        auth = {"flavor": 1,
                        "machine_name": "host1",
                        "uid": 0,
                        "gid": 0,
                        "aux_gid": list(),
                        }

        portmap = Portmap(host, timeout=10)
        portmap.connect()

        # mount initialization
        mnt_port = portmap.getport(Mount.program, Mount.program_version)
        mount = Mount(host=host, port=mnt_port, auth=auth, timeout=10)
        mount.connect()
        exports = mount.export()
        mount.disconnect()
        portmap.disconnect()
        return exports
    except:
        return None

def get_hostname_for_ip(ip):
    try:
        data = socket.gethostbyaddr(ip)
        host = data[0]
        return host
    except Exception:
        return ip

def determine_vulnerable_ips(host):
    pattern = r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:/\d{1,2}|)"
    pinged_hosts = {}
    for share in host["shares"]:
        share["share"]["vulnerable_ips"] = []
        share["share"]["vulnerable_hostnames"] = []
        share["host"] = {
            "ip" : host["host"]["ip"],
            "name" : host["host"]["name"]
        }
        share["last_seen"] = datetime.datetime.utcnow().replace(microsecond=0).isoformat()
        share["identifier"] = re.sub("[^0-9a-zA-Z]+", "-", share["host"]["name"]+"-"+share["share"]["name"])
        for client in share["share"]["clients"]:
            if client == "(everybody)":
                share["open_for_everybody"] = True
            elif re.search(pattern, client):
                ipnetwork = IPNetwork(client)
                if ipnetwork.size < 4097:
                    for ip in ipnetwork:
                        p = cached_ping(str(ip))
                        if  p == False or p == None:
                            share["share"]["vulnerable_ips"].append(str(ip), pinged_hosts)
            else:
                share["share"]["vulnerable_hostnames"].append(client)
            share["share"]["vulnerable_ips_count"] = len(share["share"]["vulnerable_ips"])
    return host

def scan(file:str, opensearchurl:str):
    with open(file, 'r') as jsonfile:
        hosts_from_file = jsonfile.readlines()
        hosts = []
        for host in hosts_from_file:
            host = host.strip()
            print(f"Scanning {host}")

            dirs = []
            exports = get_exports(host)
            
            if exports != None:
                exportdrill(exports, dirs)
            hostname = get_hostname_for_ip(host)
            newhost = {
                "host" : {
                    "ip" : host,
                    "name" : hostname
                },
                "shares" : dirs
            }
            determine_vulnerable_ips(newhost)
            flush(newhost, opensearchurl)
    return hosts

def cached_ping(ip, pinged_hosts):
    if not ip in pinged_hosts:
        p = ping(str(ip), timeout=0.2)
        pinged_hosts[ip] = p
    return pinged_hosts[ip]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script scan for open NFS-shares")
    parser.add_argument('-opensearchurl', required=True, help="url of opensearch with basic auth included, i.e. https://user:pass-@url-cluster.wur.nl:443/log-shares-nfs")
    parser.add_argument('-hosts', required=True, help="File containing hosts with nfs-shares seperated by newlines")
    args = parser.parse_args()
    inputfile = args.hosts
    opensearchurl = args.opensearchurl

    hosts = scan(inputfile, opensearchurl)
    delete_old_shares(opensearchurl)