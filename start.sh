#!/bin/bash
DIR="$( cd "$( dirname "$0" )" && pwd )"
rm /data/hosts.up -f
masscan -p 2049 --rate=400000 192.168.0.0/16 -oL - | awk '{print $4}' >> /data/hosts.up
masscan -p 2049 --rate=400000 10.0.0.0/8 -oL - | awk '{print $4}' >> /data/hosts.up
ex -s +'v/\S/d' -cwq /data/hosts.up
python3 $DIR/parse.py -opensearchurl=https://USER:PASSWORD@CLUSTERURL:443/INDEX -hosts=data/hosts.up